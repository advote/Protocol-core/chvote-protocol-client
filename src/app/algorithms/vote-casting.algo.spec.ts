import { EncryptionGroup } from "../model/encryption-group";
import { IdentificationGroup } from "../model/identification-group";
import { SecurityParameters } from "../model/security-parameters";
import { PublicParameters } from "../model/public-parameters";
import { Hash } from "./hash.algo";
import { RandomGenerator } from "./random-generator.algo";
import { GeneralAlgorithms } from "./general-algorithms.algo";
import { VoteCasting } from "./vote-casting.algo";
import { BallotQueryAndRand } from "../model/ballot-query-and-rand";
import { EncryptionPublicKey } from "../model/encryption-public-key";
import { BigIntPair } from "../model/big-int-pair";
import { NonInteractiveZkp } from "../model/non-interactive-zkp";
import { PrimesCache } from "../model/primes-cache";
import { ObliviousTransfertQuery } from "../model/oblivious-transfert-query";
import { CiphertextMatrix } from "../model/ciphertext-matrix";
import { ObliviousTransferResponse } from "../model/oblivious-transfer-response";
import { Point } from "../model/point";
import { BigInteger } from "../model/big-integer";
import * as _ from "underscore";
import { Map } from "immutable";

describe('VoteCasting', () => {

  const ZERO: BigInteger = BigInteger.ZERO;
  const ONE: BigInteger = BigInteger.ONE;
  const TWO: BigInteger = BigInteger.TWO;
  const THREE: BigInteger = new BigInteger(3);
  const FOUR: BigInteger = new BigInteger(4);
  const FIVE: BigInteger = new BigInteger(5);
  const NINE: BigInteger = new BigInteger(9);
  const ELEVEN: BigInteger = new BigInteger(11);

  const DEFAULT_ALPHABET: Uint8Array = _.map('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_'.split(''), function (char: string) {
    return char.charCodeAt(0);
  });

  const ENCRYPTION_GROUP: EncryptionGroup = new EncryptionGroup(ELEVEN, FIVE, THREE, FOUR);
  const IDENTIFICATION_GROUP: IdentificationGroup = new IdentificationGroup(ELEVEN, FIVE, THREE);
  const SECURITY_PARAMETERS: SecurityParameters = new SecurityParameters(1, 1, 2, 0.99);

  const PUBLIC_PARAMETERS: PublicParameters = new PublicParameters(
    SECURITY_PARAMETERS,
    ENCRYPTION_GROUP,
    IDENTIFICATION_GROUP,
    FIVE,
    FIVE,
    DEFAULT_ALPHABET,
    FIVE,
    DEFAULT_ALPHABET,
    DEFAULT_ALPHABET,
    2,
    DEFAULT_ALPHABET,
    2,
    2,
    5
  );

  let hashMock: Hash;
  let generalAlgorithmsMock: GeneralAlgorithms;
  let voteCastingClient: VoteCasting;

  beforeAll(() => {
    hashMock = new Hash(null, 0);
    generalAlgorithmsMock = new GeneralAlgorithms(null, ENCRYPTION_GROUP, IDENTIFICATION_GROUP, new PrimesCache([], ENCRYPTION_GROUP));
    voteCastingClient = new VoteCasting(PUBLIC_PARAMETERS, generalAlgorithmsMock, hashMock);
  });

  it('genBallot should generate a valid ballot (incl. OT query and used randomness)', () => {
    // given some known randomness
    spyOn(RandomGenerator, 'randomInZq').and.returnValues(
      ONE,   // genQuery, r_1
      THREE, // genBallotProof, omega_1
      ONE    // genBallotProof, omega_3
    );
    spyOn(RandomGenerator, 'randomInGq').and.returnValue(FIVE); // genBallotProof, omega_2

    // and some valid selected primes
    spyOn(generalAlgorithmsMock, 'getPrimes').and.returnValue([THREE]);

    // and some arbitrary values for the proof challenge
    // t_1 = g_hat ^ omega_1 mod p_hat = 3 ^ 3 mod 11 = 5
    // t_2 = omega_2 * pk ^ omega_3 mod p = 5 * 3 ^ 1 mod 11 = 4
    // t_3 = g ^ omega_3 mod p = 3 ^ 1 mod 11 = 3
    spyOn(generalAlgorithmsMock, 'getNIZKPChallenge').and.returnValue(ONE);

    // and the expected preconditions check
    spyOn(generalAlgorithmsMock, 'isMember_G_q_hat').and.returnValue(true);
    spyOn(generalAlgorithmsMock, 'isMember').and.returnValue(true);
    spyOn(generalAlgorithmsMock, 'isInZ_q').and.callThrough();
    spyOn(generalAlgorithmsMock, 'isInZ_q_hat').and.callThrough();

    // when generating a ballot
    let ballotQueryAndRand: BallotQueryAndRand = voteCastingClient.genBallot('a', [1], new EncryptionPublicKey(THREE, ENCRYPTION_GROUP));

    // then x_hat has the expected value
    // x = 5
    // x_hat = g_hat ^ x mod p_hat = 3 ^ 5 mod 11 = 1
    expect(ballotQueryAndRand.alpha.x_hat.equals(ONE)).toBeTruthy(`x_hat: expected ${ballotQueryAndRand.alpha.x_hat.toHexString()} to equal 1`);

    // and bold_a has the expected value
    // m = 3
    // a_1_1 = q_1 * pk ^ r_1 mod p = 3 * 3 ^ 1 mod 11 = 9
    // a_1_2 = g ^ r_1 mod p = 3 ^ 1 mod 11 = 3
    expect(ballotQueryAndRand.alpha.bold_a.length).toBe(1);
    expect(ballotQueryAndRand.alpha.bold_a[0].left.equals(NINE)).toBeTruthy(`bold_a left: expected ${ballotQueryAndRand.alpha.bold_a[0].left.toHexString()} to equal 9`);
    expect(ballotQueryAndRand.alpha.bold_a[0].right.equals(THREE)).toBeTruthy(`bold_a right: expected ${ballotQueryAndRand.alpha.bold_a[0].right.toHexString()} to equal 3`);

    // and pi has the expected value
    // for values of t_1 to t_3 see above
    // s_1 = omega_1 + c * x mod q_hat = 3 + 1 * 15 mod 5 = 3
    // s_2 = omega_2 * m ^ c mod p = 5 * 3 ^ 1 mod 11 = 4
    // s_3 = omega_3 + c * r mod q = 1 + 1 * 1 mod 5 = 2
    expect(ballotQueryAndRand.alpha.pi.t.length).toBe(3);
    expect(ballotQueryAndRand.alpha.pi.t[0].equals(FIVE)).toBeTruthy(`pi t[0]: expected ${ballotQueryAndRand.alpha.pi.t[0].toHexString()} to equal 5`);
    expect(ballotQueryAndRand.alpha.pi.t[1].equals(FOUR)).toBeTruthy(`pi t[1]: expected ${ballotQueryAndRand.alpha.pi.t[1].toHexString()} to equal 4`);
    expect(ballotQueryAndRand.alpha.pi.t[2].equals(THREE)).toBeTruthy(`pi t[2]: expected ${ballotQueryAndRand.alpha.pi.t[2].toHexString()} to equal 3`);

    expect(ballotQueryAndRand.alpha.pi.s.length).toBe(3);
    expect(ballotQueryAndRand.alpha.pi.s[0].equals(THREE)).toBeTruthy(`pi s[0]: expected ${ballotQueryAndRand.alpha.pi.s[0].toHexString()} to equal 3`);
    expect(ballotQueryAndRand.alpha.pi.s[1].equals(FOUR)).toBeTruthy(`pi s[1]: expected ${ballotQueryAndRand.alpha.pi.s[1].toHexString()} to equal 4`);
    expect(ballotQueryAndRand.alpha.pi.s[2].equals(TWO)).toBeTruthy(`pi s[2]: expected ${ballotQueryAndRand.alpha.pi.s[2].toHexString()} to equal 2`);

    // and the provided randomness is returned
    expect(ballotQueryAndRand.bold_r.length).toBe(1);
    expect(ballotQueryAndRand.bold_r[0].equals(ONE)).toBeTruthy(`bold_r: expected ${ballotQueryAndRand.bold_r[0].toHexString()} to equal 1`);
  });

  it('getSelectedPrimes', () => {
    // given some valid selected primes
    spyOn(generalAlgorithmsMock, 'getPrimes').and.returnValue([THREE, FIVE, ELEVEN]);

    // when
    let selectedPrimes: BigInteger[] = voteCastingClient.getSelectedPrimes([2]);

    // then
    expect(selectedPrimes.length).toBe(1);
    expect(selectedPrimes[0].equals(FIVE)).toBeTruthy(`expected ${selectedPrimes[0].toHexString()} to equal 5`);
  });

  it('genQuery should generate a valid query for the ballot (incl. the randomness used)', () => {
    // given some known randomness
    spyOn(RandomGenerator, 'randomInZq').and.returnValue(ONE);

    // and the expected preconditions checks
    spyOn(generalAlgorithmsMock, 'isMember').and.returnValue(true);

    // when generating a query
    let query: ObliviousTransfertQuery = voteCastingClient.genQuery([THREE], new EncryptionPublicKey(THREE, ENCRYPTION_GROUP));

    // then
    // a_1,1 = q_1 * pk ^ r_1 mod p = 3 * 3 ^ 1 mod 11 = 9
    // a_1,2 = g ^ r_1 mod p = 3 ^ 1 mod 11 = 3
    expect(query.bold_a.length).toBe(1);
    expect(query.bold_a[0].left.equals(NINE)).toBeTruthy(`left part: expected ${query.bold_a[0].left.toHexString()} to equal 9`);
    expect(query.bold_a[0].right.equals(THREE)).toBeTruthy(`right part: expected ${query.bold_a[0].right.toHexString()} to equal 3`);

    expect(query.bold_r.length).toBe(1);
    expect(query.bold_r[0].equals(ONE)).toBeTruthy(`expected ${query.bold_r[0].toHexString()} to equal 1`);
  });

  it('genBallotProof should generate a valid proof of knowledge of the ballot', () => {
    // given some known randomness
    spyOn(RandomGenerator, 'randomInZq').and.returnValues(THREE, ONE); // omega_1 and omega_3
    spyOn(RandomGenerator, 'randomInGq').and.returnValue(FIVE); // omega_2

    // and some arbitrary values for the proof challenge
    // t_1 = g_hat ^ omega_1 mod p_hat = 3 ^ 3 mod 11 = 5
    // t_2 = omega_2 * pk ^ omega_3 mod p = 5 * 3 ^ 1 mod 11 = 4
    // t_3 = g ^ omega_3 mod p = 3 ^ 1 mod 11 = 3
    spyOn(generalAlgorithmsMock, 'getNIZKPChallenge').and.returnValue(ONE); // c

    // and the expected preconditions verifications
    spyOn(generalAlgorithmsMock, 'isMember_G_q_hat').and.returnValue(true);
    spyOn(generalAlgorithmsMock, 'isMember').and.returnValue(true);
    spyOn(generalAlgorithmsMock, 'isInZ_q').and.callThrough();
    spyOn(generalAlgorithmsMock, 'isInZ_q_hat').and.callThrough();

    // when generating a ballot ZKP
    let pi: NonInteractiveZkp = voteCastingClient.genBallotProof(
      ZERO,
      THREE,
      ONE,
      ONE,
      [new BigIntPair(NINE, THREE)],
      new EncryptionPublicKey(THREE, ENCRYPTION_GROUP)
    );

    // then
    // for values of t_1 to t_3 see above
    // s_1 = omega_1 + c * x mod q_hat = 3 + 1 * 5 mod 5 = 3
    // s_2 = omega_2 * m ^ c mod p = 5 * 3 ^ 1 mod 11 = 4
    // s_3 = omega_3 + c * r mod q = 1 + 1 * 1 mod 3 = 2
    expect(pi.t.length).toBe(3);
    expect(pi.t[0].equals(FIVE)).toBeTruthy(`expected ${pi.t[0].toHexString()} to equal 5`);
    expect(pi.t[1].equals(FOUR)).toBeTruthy(`expected ${pi.t[1].toHexString()} to equal 4`);
    expect(pi.t[2].equals(THREE)).toBeTruthy(`expected ${pi.t[2].toHexString()} to equal 3`);

    expect(pi.s.length).toBe(3);
    expect(pi.s[0].equals(THREE)).toBeTruthy(`expected ${pi.s[0].toHexString()} to equal 3`);
    expect(pi.s[1].equals(FOUR)).toBeTruthy(`expected ${pi.s[1].toHexString()} to equal 4`);
    expect(pi.s[2].equals(TWO)).toBeTruthy(`expected ${pi.s[2].toHexString()} to equal 2`);
  });

  let innerMap = function (numbers: number[]): Map<number, Uint8Array> {
    let innerMap: Map<number, Uint8Array> = Map();
    innerMap = innerMap.set(0, new Uint8Array(numbers));
    return innerMap;
  };

  it('getPointMatrix should compute the point matrix according to spec', () => {
    // given
    let b1: BigInteger[] = [ONE];

    let c1Map: Map<number, Map<number, Uint8Array>> = Map();
    c1Map = c1Map.set(0, innerMap([0x01, 0x02]));
    c1Map = c1Map.set(1, innerMap([0x05, 0x06]));
    c1Map = c1Map.set(2, innerMap([0x0A, 0x0B]));
    let c1: CiphertextMatrix = new CiphertextMatrix(c1Map);

    let beta_1: ObliviousTransferResponse = new ObliviousTransferResponse(b1, c1, THREE);

    let b2: BigInteger[] = [FIVE];

    let c2Map: Map<number, Map<number, Uint8Array>> = Map();
    c2Map = c2Map.set(0, innerMap([0x10, 0x20]));
    c2Map = c2Map.set(1, innerMap([0x50, 0x60]));
    c2Map = c2Map.set(2, innerMap([0xA0, 0xB0]));
    let c2: CiphertextMatrix = new CiphertextMatrix(c2Map);

    let beta_2: ObliviousTransferResponse = new ObliviousTransferResponse(b2, c2, FOUR);

    spyOn(hashMock, 'recHash_L').and.returnValues(
      [0x0E, 0x0A], // Authority 1: b_i * d^{-r_i} mod p = 1 * 3^-0 mod 11 = 1
      [0xA3, 0xB0]         // Authority 2: b_i * d^{-r_i} mod p = 5 * 4^-0 mod 11 = 5
    );

    // and the expected preconditions checks
    spyOn(generalAlgorithmsMock, 'isMember').and.returnValue(true);
    spyOn(generalAlgorithmsMock, 'isInZ_q').and.callThrough();

    // when
    let pointMatrix: Point[][] = voteCastingClient.getPointMatrix([beta_1, beta_2], [3], [ZERO]);

    // then
    expect(pointMatrix).toEqual([
      [new Point(FOUR, ONE)],  // Authority 1
      [new Point(THREE, ZERO)] // Authority 2
    ]);
  });

  it(`getPoints should compute the points correctly from the authority's reply`, () => {
    // given
    let b: BigInteger[] = [ONE];

    let cMap: Map<number, Map<number, Uint8Array>> = Map();
    cMap = cMap.set(0, innerMap([0x01, 0x02]));
    cMap = cMap.set(1, innerMap([0x05, 0x06]));
    cMap = cMap.set(2, innerMap([0x0A, 0x0B]));
    let c: CiphertextMatrix = new CiphertextMatrix(cMap);

    let beta: ObliviousTransferResponse = new ObliviousTransferResponse(b, c, THREE);
    spyOn(hashMock, 'recHash_L').and.returnValue([0x0E, 0x0A]);   // b_i * d^{-r_i} mod p = 1 * 3^-0 mod 11 = 1

    // and the expected preconditions checks
    spyOn(generalAlgorithmsMock, 'isMember').and.returnValue(true);

    // when
    let points: Point[] = voteCastingClient.getPoints(beta, [3], [FIVE]);

    // then
    expect(points).toEqual([new Point(FOUR, ONE)]);
  });

  it('getReturnCodes should combine the given point matrix into the verification codes for the voter', () => {
    // given
    let point11: Point = new Point(ONE, FOUR);
    let point21: Point = new Point(FIVE, THREE);
    let pointMatrix: Point[][] = [
      [ // authority 1
        point11 // choice 1
      ],
      [ // authority 2
        point21 // choice 1
      ]
    ];

    // spyOn(hashMock, 'recHash_L').and.callFake(function (point: Point) {
    //   if (point === point11) {
    //     return [0x05, 0x06];
    //   } else {
    //     return [0xD1, 0xCF];
    //   }
    // });

    spyOn(hashMock, 'recHash_L').and.returnValues([0x05, 0x06], [0xD1, 0xCF]);

    // when
    let rc: string[] = voteCastingClient.getReturnCodes([1], pointMatrix);

    // then
    expect(rc.length).toBe(1);
    expect(rc[0]).toBe('ntj'); // [0xD4, 0xC9] -> 54473
  });

});
