import { Conversion } from './conversion.algo';
import { BigInteger } from "../model/big-integer";
import * as _ from "underscore";

describe('Conversion', () => {

  const RANGE_0_1: Uint8Array = _.map('01'.split(''), function (char: string) {
    return char.charCodeAt(0);
  });
  const RANGE_A_Z: Uint8Array = _.map('ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split(''), function (char: string) {
    return char.charCodeAt(0);
  });

  // toByteArray(LargeInteger, number)
  [
    [0, 0, []],
    [0, 1, [0x00]],
    [0, 2, [0x00, 0x00]],
    [0, 3, [0x00, 0x00, 0x00]],
    [255, 1, [0xFF]],
    [255, 2, [0x00, 0xFF]],
    [255, 3, [0x00, 0x00, 0xFF]],
    [256, 2, [0x01, 0x00]],
    [256, 3, [0x00, 0x01, 0x00]],
    [256, 4, [0x00, 0x00, 0x01, 0x00]]
  ].forEach(([_x, n, bytes]) => {
    it(`toByteArray(${_x}, ${n}) should return [${bytes}]`, () => {
      let x: BigInteger = new BigInteger(<number>_x);
      let expectedBytes: Uint8Array = new Uint8Array(<number[]>bytes);
      let actualBytes = Conversion.toByteArray(x, <number>n);
      expect(actualBytes.toString()).toEqual(expectedBytes.toString());
    });
  });

  // toByteArray(LargeInteger)
  [
    [0, []],
    [1, [0x1]],
    [255, [0xFF]],
    [256, [0x01, 0x00]],
    [65535, [0xFF, 0xFF]],
    [65536, [0x01, 0x00, 0x00]],
    [16777215, [0xFF, 0xFF, 0xFF]],
    [16777216, [0x01, 0x00, 0x00, 0x00]]
  ].forEach(([_x, bytes]) => {
    it(`toByteArray(${_x}) should return [${bytes}]`, () => {
      let x: BigInteger = new BigInteger(<number>_x);
      let expectedBytes: Uint8Array = new Uint8Array(<number[]>bytes);
      let actualBytes = Conversion.toByteArray(x);
      expect(actualBytes.toString()).toEqual(expectedBytes.toString());
    });
  });

  // toByteArray(string)
  [
    ['Hello', [0x48, 0x65, 0x6C, 0x6C, 0x6F]],
    ['Voilà', [0x56, 0x6F, 0x69, 0x6C, 0xC3, 0xA0]]
  ].forEach(([x, _bytes]) => {
    it(`toByteArray("${x}") should return [${_bytes}]`, () => {
      let bytes: Uint8Array = new Uint8Array(<number[]>_bytes);
      let actual = Conversion.toByteArray(<string>x);
      expect(actual).toEqual(bytes, `actual = ${actual}`);
    });
  });

  // toInteger()
  [1, 128, 12938765425438].forEach((x) => {
    it(`toInteger(toByteArray(${x})) should return [${x}]`, () => {
      let expected: BigInteger = new BigInteger(<number>x);
      let actual = Conversion.toInteger(Conversion.toByteArray(expected));
      expect(actual.toHexString()).toEqual(expected.toHexString());
    });
  });

  // toInteger(string, number[])
  [
    ['', 'range01', RANGE_0_1, 0],
    ['0001', 'range 0-1', RANGE_0_1, 1],
    ['1000', 'range 0-1', RANGE_0_1, 8],
    ['1111', 'range 0-1', RANGE_0_1, 15],
    ['Z', 'range A-Z', RANGE_A_Z, 25],
    ['ZA', 'range A-Z', RANGE_A_Z, 650],
    ['ZZ', 'range A-Z', RANGE_A_Z, 675],
    ['ZAA', 'range A-Z', RANGE_A_Z, 16900],
    ['ZZZ', 'range A-Z', RANGE_A_Z, 17575],
    ['ZAAA', 'range A-Z', RANGE_A_Z, 439400],
    ['ZZZZ', 'range A-Z', RANGE_A_Z, 456975]
  ].forEach(([s, rangeDesc, range, _x]) => {
    it(`toInteger(${s}, [${rangeDesc}]) should return [${_x}]`, () => {
      let x: BigInteger = new BigInteger(<number>_x);
      expect(Conversion.toInteger(<string>s, <Uint8Array>range).equals(x)).toBeTruthy();
    });
  });

  // toString(LargeInteger, number[], number)
  [
    [0, 'range 0-1', RANGE_0_1, '00000000'],
    [1, 'range 0-1', RANGE_0_1, '00000001'],
    [2, 'range 0-1', RANGE_0_1, '00000010'],
    [4, 'range 0-1', RANGE_0_1, '00000100'],
    [8, 'range 0-1', RANGE_0_1, '00001000'],
    [15, 'range 0-1', RANGE_0_1, '00001111'],
    [731, 'range A-Z', RANGE_A_Z, 'ABCD'],
    [25, 'range A-Z', RANGE_A_Z, 'AZ'],
    [650, 'range A-Z', RANGE_A_Z, 'AAZA'],
    [675, 'range A-Z', RANGE_A_Z, 'AAZZ'],
    [16900, 'range A-Z', RANGE_A_Z, 'AZAA'],
    [17575, 'range A-Z', RANGE_A_Z, 'AZZZ'],
    [439400, 'range A-Z', RANGE_A_Z, 'AAZAAA'],
    [456975, 'range A-Z', RANGE_A_Z, 'AAZZZZ']
  ].forEach(([_x, ADesc, A, s]) => {
    it(`toString(${_x}, [${ADesc}]) should return "${s}"`, () => {
      let x: BigInteger = new BigInteger(<number>_x);
      expect(Conversion.toString(x.toByteArray(), <Uint8Array>A)).toEqual(<string>s);
    });
  });

});
