import { BallotQueryAndRand } from "../model/ballot-query-and-rand";
import { EncryptionPublicKey } from "../model/encryption-public-key";
import { PublicParameters } from "../model/public-parameters";
import { GeneralAlgorithms } from "./general-algorithms.algo";
import { RandomGenerator } from "./random-generator.algo";
import { Hash } from "./hash.algo";
import { Conversion } from "./conversion.algo";
import { ObliviousTransfertQuery } from "../model/oblivious-transfert-query";
import { NonInteractiveZkp } from "../model/non-interactive-zkp";
import { ObliviousTransferResponse } from "../model/oblivious-transfer-response";
import { Point } from "../model/point";
import { BigIntPair } from "../model/big-int-pair";
import { Preconditions } from "../tools/preconditions";
import { BallotAndQuery } from "../model/ballot-and-query";
import { IdentificationGroup } from "../model/identification-group";
import { EncryptionGroup } from "../model/encryption-group";
import { CiphertextMatrix } from "../model/ciphertext-matrix";
import { BigInteger } from "../model/big-integer";
import * as _ from "underscore";

export class VoteCasting {

  private static THERE_NEEDS_TO_BE_AT_LEAST_ONE_SELECTION: string = 'There needs to be at least one selection';
  private static SELECTIONS_MUST_BE_STRICTLY_POSITIVE: string = 'Selections must be strictly positive';
  private static ALL_SELECTIONS_MUST_BE_DISTINCT: string = 'All selections must be distinct';
  private static THE_KEY_MUST_BE_A_MEMBER_OF_G_Q: string = 'The key must be a member of G_q';
  private static THE_ELEMENTS_MUST_BE_SORTED: string = 'The elements must be sorted';

  constructor(private publicParameters: PublicParameters,
              private generalAlgorithms: GeneralAlgorithms,
              private hash: Hash) {
  }

  /**
   * Algorithm 7.18: GenBallot
   *
   * @param upper_x the voting code
   * @param bold_s voters selection (indices)
   * @param pk the public encryption key
   * @returns the combined ballot, OT query and random elements used
   */
  public genBallot(upper_x: string, bold_s: number[], pk: EncryptionPublicKey): BallotQueryAndRand {
    Preconditions.checkArgument(bold_s.length > 0, VoteCasting.THERE_NEEDS_TO_BE_AT_LEAST_ONE_SELECTION);
    Preconditions.checkArgument(_.isEqual(this.copyAndSort(bold_s), bold_s), VoteCasting.THE_ELEMENTS_MUST_BE_SORTED);
    Preconditions.checkArgument(_.every(bold_s, function (num: number) {
      return num >= 1;
    }), VoteCasting.SELECTIONS_MUST_BE_STRICTLY_POSITIVE);
    Preconditions.checkArgument(Array.from(new Set(bold_s)).length === bold_s.length, VoteCasting.ALL_SELECTIONS_MUST_BE_DISTINCT);
    Preconditions.checkArgument(this.generalAlgorithms.isMember(pk.publicKey), VoteCasting.THE_KEY_MUST_BE_A_MEMBER_OF_G_Q);
    Preconditions.checkArgument(BigInteger.ONE.cmp(pk.publicKey) != 0, 'The key must not be 1');

    let p_hat: BigInteger = this.publicParameters.identificationGroup.p_hat;
    let g_hat: BigInteger = this.publicParameters.identificationGroup.g_hat;
    let p: BigInteger = this.publicParameters.encryptionGroup.p;
    let q: BigInteger = this.publicParameters.encryptionGroup.q;

    let x: BigInteger = Conversion.toInteger(upper_x, this.publicParameters.upper_a_x);
    let x_hat: BigInteger = g_hat.modPow(x, p_hat);

    let bold_q: BigInteger[] = this.computeBoldQ(bold_s);
    let query: ObliviousTransfertQuery = this.genQuery(bold_q, pk);
    let m: BigInteger = VoteCasting.computeM(bold_q, p);
    let r: BigInteger = VoteCasting.computeR(query, q);
    let pi: NonInteractiveZkp = this.genBallotProof(x, m, r, x_hat, query.bold_a, pk);
    let alpha: BallotAndQuery = new BallotAndQuery(x_hat, query.bold_a, pi);

    return new BallotQueryAndRand(alpha, query.bold_r);
  }

  private computeBoldQ(bold_s: number[]): BigInteger[] {
    let bold_q: BigInteger[];
    try {
      bold_q = this.getSelectedPrimes(bold_s);
    } catch (error) {
      throw new Error('Encryption Group too small for selection');
    }
    return bold_q;
  }

  private static computeM(bold_q: BigInteger[], p: BigInteger): BigInteger {
    let m: BigInteger = BigInteger.ONE;
    for (let q of bold_q) {
      m = m.mul(q);
    }
    if (m.cmp(p) >= 0) {
      throw new Error("(k,n) is incompatible with p");
    }
    return m;
  }

  private static computeR(query: ObliviousTransfertQuery, q: BigInteger): BigInteger {
    let a: BigInteger = BigInteger.ZERO;
    for (let r of query.bold_r) {
      a = a.add(r);
    }
    return a.mod(q);
  }

  /**
   * Algorithm 7.19: getSelectedPrimes
   *
   * @param bold_s the indices of the selected index (in increasing order, 1-based)
   * @return the list of the primes selected
   * @throws Error if there are not enough primes.
   */
  public getSelectedPrimes(bold_s: number[]): BigInteger[] {
    Preconditions.checkArgument(bold_s.length > 0, VoteCasting.THERE_NEEDS_TO_BE_AT_LEAST_ONE_SELECTION);
    Preconditions.checkArgument(_.isEqual(this.copyAndSort(bold_s), bold_s), VoteCasting.THE_ELEMENTS_MUST_BE_SORTED);
    Preconditions.checkArgument(_.every(bold_s, function (num: number) {
      return num >= 1;
    }), VoteCasting.SELECTIONS_MUST_BE_STRICTLY_POSITIVE);
    Preconditions.checkArgument(Array.from(new Set(bold_s)).length === bold_s.length, VoteCasting.ALL_SELECTIONS_MUST_BE_DISTINCT);

    let s_k: number = bold_s[bold_s.length - 1];
    let primes: BigInteger[] = this.generalAlgorithms.getPrimes(s_k);

    let selectedPrimes: BigInteger[] = [];
    for (let i: number = 0; i < bold_s.length; i++) {
      selectedPrimes.push(primes[bold_s[i] - 1]); // bold_s is 1-based
    }
    return selectedPrimes;
  }

  /**
   * Algorithm 7.20: GenQuery
   *
   * @param bold_q the selected primes
   * @param pk the public encryption key
   * @return the generated oblivious transfer query
   */
  public genQuery(bold_q: BigInteger[], pk: EncryptionPublicKey): ObliviousTransfertQuery {
    Preconditions.checkArgument(this.generalAlgorithms.isMember(pk.publicKey), VoteCasting.THE_KEY_MUST_BE_A_MEMBER_OF_G_Q);
    Preconditions.checkArgument(BigInteger.ONE.cmp(pk.publicKey) != 0, 'The key must not be 1');

    let g: BigInteger = this.publicParameters.encryptionGroup.g;
    let q: BigInteger = this.publicParameters.encryptionGroup.q;
    let p: BigInteger = this.publicParameters.encryptionGroup.p;

    let bold_a: BigIntPair[] = [];
    let bold_r: BigInteger[] = [];

    for (let q_j of bold_q) {
      let r_j: BigInteger = RandomGenerator.randomInZq(q);
      let a_j_1: BigInteger = q_j.mul(pk.publicKey.modPow(r_j, p)).mod(p);
      let a_j_2: BigInteger = g.modPow(r_j, p);
      bold_a.push(new BigIntPair(a_j_1, a_j_2));
      bold_r.push(r_j);
    }
    return new ObliviousTransfertQuery(bold_a, bold_r);
  }

  /**
   * Algorithm 7.21: GenBallotProof
   *
   * @param x first half of voting credentials
   * @param m encoded selections, m is in G_q
   * @param r randomization
   * @param x_hat second half of voting credentials
   * @param bold_a the oblivious transfer query containing the encrypted vote
   * @param pk encryption key
   * @return a non interactive proof of knowledge for the ballot
   */
  public genBallotProof(x: BigInteger,
                        m: BigInteger,
                        r: BigInteger,
                        x_hat: BigInteger,
                        bold_a: BigIntPair[],
                        pk: EncryptionPublicKey): NonInteractiveZkp {
    Preconditions.checkArgument(this.generalAlgorithms.isInZ_q_hat(x), 'The private credential must be in Z_q_hat');
    Preconditions.checkArgument(this.generalAlgorithms.isMember_G_q_hat(x_hat), 'x_hat must be in G_q_hat');
    Preconditions.checkArgument(this.generalAlgorithms.isMember(m), 'm must be in G_q');
    Preconditions.checkArgument(this.generalAlgorithms.isInZ_q(r), 'r must be in Z_q');

    let genAlg = this.generalAlgorithms;
    Preconditions.checkArgument(_.every(bold_a, function (pair: BigIntPair) {
      return genAlg.isMember(pair.left) && genAlg.isMember(pair.right);
    }), 'all a_j 1 and 2 must be in G_q');

    Preconditions.checkArgument(this.generalAlgorithms.isMember(pk.publicKey), VoteCasting.THE_KEY_MUST_BE_A_MEMBER_OF_G_Q);

    let identificationGroup: IdentificationGroup = this.publicParameters.identificationGroup;
    let p_hat: BigInteger = identificationGroup.p_hat;
    let q_hat: BigInteger = identificationGroup.q_hat;
    let g_hat: BigInteger = identificationGroup.g_hat;

    let encryptionGroup: EncryptionGroup = this.publicParameters.encryptionGroup;
    let p: BigInteger = encryptionGroup.p;
    let q: BigInteger = encryptionGroup.q;
    let g: BigInteger = encryptionGroup.g;

    let tau: number = this.publicParameters.securityParameters.tau;

    let omega_1: BigInteger = RandomGenerator.randomInZq(q_hat);
    let omega_2: BigInteger = RandomGenerator.randomInGq(encryptionGroup);
    let omega_3: BigInteger = RandomGenerator.randomInZq(q);

    let t_1: BigInteger = g_hat.modPow(omega_1, p_hat);
    let t_2: BigInteger = omega_2.mul(pk.publicKey.modPow(omega_3, p)).mod(p);
    let t_3: BigInteger = g.modPow(omega_3, p);

    let y: any[] = [x_hat, bold_a];
    let t: BigInteger[] = [t_1, t_2, t_3];
    let c: BigInteger = this.generalAlgorithms.getNIZKPChallenge(y, t, tau);

    let s_1: BigInteger = omega_1.add(c.mul(x)).mod(q_hat);
    let s_2: BigInteger = omega_2.mul(m.modPow(c, p)).mod(p);
    let s_3: BigInteger = omega_3.add(c.mul(r)).mod(q);
    let s: BigInteger[] = [s_1, s_2, s_3];

    return new NonInteractiveZkp(t, s);
  }

  /**
   * Algorithm 7.26: GetPointMatrix
   *
   * @param bold_beta the vector of the oblivious transfer replies (from the different authorities)
   * @param bold_s    the vector of selected primes
   * @param bold_r    the vector of randomizations used for the OT query
   * @return the point matrix corresponding to the replies of the s authorities for the k selections
   * @throws Error when one of the points would be outside the defined space
   */
  public getPointMatrix(bold_beta: ObliviousTransferResponse[],
                        bold_s: number[],
                        bold_r: BigInteger[]): Point[][] {
    let genAlg = this.generalAlgorithms;
    Preconditions.checkArgument(_.every(bold_beta, function (response: ObliviousTransferResponse) {
      return _.every(response.bold_b, function (b: BigInteger) {
        return genAlg.isMember(b);
      })
    }), `All the b_i's in bold_beta must be in G_q`);

    Preconditions.checkArgument(_.every(bold_beta, function (response: ObliviousTransferResponse) {
      return genAlg.isMember(response.d);
    }), `All the d's in bold_beta must be in G_q`);

    Preconditions.checkArgument(bold_s.length > 0, VoteCasting.THERE_NEEDS_TO_BE_AT_LEAST_ONE_SELECTION);
    Preconditions.checkArgument(_.isEqual(this.copyAndSort(bold_s), bold_s), VoteCasting.THE_ELEMENTS_MUST_BE_SORTED);
    Preconditions.checkArgument(_.every(bold_s, function (num: number) {
      return num >= 1;
    }), VoteCasting.SELECTIONS_MUST_BE_STRICTLY_POSITIVE);
    Preconditions.checkArgument(Array.from(new Set(bold_s)).length === bold_s.length, VoteCasting.ALL_SELECTIONS_MUST_BE_DISTINCT);

    Preconditions.checkArgument(_.every(bold_r, function (r: BigInteger) {
      return genAlg.isInZ_q(r);
    }), `All r_j must be in Z_q`);

    let bold_P: Point[][] = [];

    for (let beta_i of bold_beta) {
      bold_P.push(this.getPoints(beta_i, bold_s, bold_r));
    }

    return bold_P;
  }

  /**
   * Algorithm 7.27: GetPoints
   *
   * @param beta   the OT response (from one authority)
   * @param bold_s the vector of selected primes
   * @param bold_r the vector of randomizations used for the OT query
   * @return the points corresponding to the authority's reply for the k selections
   * @throws Error when one of the points would be outside the defined space
   */
  public getPoints(beta: ObliviousTransferResponse,
                   bold_s: number[],
                   bold_r: BigInteger[]): Point[] {
    let genAlg = this.generalAlgorithms;
    Preconditions.checkArgument(_.every(beta.bold_b, function (b: BigInteger) {
      return genAlg.isMember(b);
    }), `All the b_j's in bold_beta must be in G_q`);

    Preconditions.checkArgument(this.generalAlgorithms.isMember(beta.d), 'd must be in G_q');

    Preconditions.checkArgument(bold_s.length > 0, VoteCasting.THERE_NEEDS_TO_BE_AT_LEAST_ONE_SELECTION);
    Preconditions.checkArgument(_.isEqual(this.copyAndSort(bold_s), bold_s), VoteCasting.THE_ELEMENTS_MUST_BE_SORTED);
    Preconditions.checkArgument(_.every(bold_s, function (num: number) {
      return num >= 1;
    }), VoteCasting.SELECTIONS_MUST_BE_STRICTLY_POSITIVE);
    Preconditions.checkArgument(Array.from(new Set(bold_s)).length === bold_s.length, VoteCasting.ALL_SELECTIONS_MUST_BE_DISTINCT);

    let bold_p: Point[] = [];
    let bold_b: BigInteger[] = beta.bold_b;
    let bold_upper_c: CiphertextMatrix = beta.bold_upper_c;
    let d: BigInteger = beta.d;
    let p: BigInteger = this.publicParameters.encryptionGroup.p;
    let p_prime: BigInteger = this.publicParameters.p_prime;
    let upper_l_m: number = this.publicParameters.upper_l_m;

    for (let j: number = 0; j < bold_s.length; j++) {
      let k: BigInteger = bold_b[j].mul(d.modInv(p).modPow(bold_r[j], p)).mod(p);
      let upper_k: Uint8Array = this.computeUpperK(upper_l_m, k);
      let upper_m: Uint8Array = GeneralAlgorithms.xor(
        // selections are 1-based
        bold_upper_c.get(bold_s[j] - 1, j),
        upper_k
      );
      let x_i: BigInteger = Conversion.toInteger(GeneralAlgorithms.extract(upper_m, 0, upper_l_m / 2));
      let y_i: BigInteger = Conversion.toInteger(GeneralAlgorithms.extract(upper_m, upper_l_m / 2, upper_m.length));

      if (x_i.cmp(p_prime) >= 0 || y_i.cmp(p_prime) >= 0) {
        throw new Error(`x_j >= p' or y_j >= p'`);
      }
      bold_p.push(new Point(x_i, y_i));
    }

    return bold_p;
  }

  private computeUpperK(upper_l_m: number, k: BigInteger): Uint8Array {
    let bold_upper_k: Uint8Array = new Uint8Array(0);
    let l_m: number = Math.ceil(upper_l_m / this.publicParameters.securityParameters.upper_l);
    for (let i: number = 1; i <= l_m; i++) {
      bold_upper_k = GeneralAlgorithms.concatenate(bold_upper_k, this.hash.recHash_L([k, new BigInteger(i)]));
    }
    return GeneralAlgorithms.truncate(bold_upper_k, upper_l_m);
  }

  /**
   * Algorithm 7.28: GetReturnCodes
   *
   * @param bold_s         the list of selections
   * @param bold_upper_p_s the point matrix containing the responses for each of the authorities
   * @return the verification codes corresponding to the point matrix
   */
  public getReturnCodes(bold_s: number[], bold_upper_p_s: Point[][]): string[] {
    let length: number = bold_upper_p_s[0].length;

    Preconditions.checkArgument(_.every(bold_upper_p_s, function (point: Point[]) {
      return point.length == length;
    }), 'All points should have the same size');

    let upper_a_r: Uint8Array = this.publicParameters.upper_a_r;

    let bold_rc_s: string[] = [];
    for (let j: number = 0; j < length; j++) {
      let rc_i: Uint8Array = new Uint8Array(this.publicParameters.upper_l_r);
      for (let i: number = 0; i < this.publicParameters.s; i++) {
        rc_i = GeneralAlgorithms.xor(rc_i,
          GeneralAlgorithms.truncate(
            this.hash.recHash_L([bold_upper_p_s[i][j].x, bold_upper_p_s[i][j].y]),
            this.publicParameters.upper_l_r
          )
        );
      }
      let upper_r_j: Uint8Array = GeneralAlgorithms.markByteArray(rc_i, bold_s[j] - 1, this.publicParameters.n_max);
      bold_rc_s.push(Conversion.toString(upper_r_j, upper_a_r));
    }
    return bold_rc_s;
  }

  private copyAndSort(selection: number[]): number[] {
    let copy: number[] = [];
    for (let i: number = 0; i < selection.length; i++) {
      copy.push(selection[i]);
    }
    return copy.sort((a, b) => a - b);
  }
}
