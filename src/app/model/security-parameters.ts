import { Preconditions } from "../tools/preconditions";

export class SecurityParameters {

  constructor(public readonly sigma: number,
              public readonly tau: number,
              public readonly upper_l: number,
              public readonly epsilon: number) {
    Preconditions.checkArgument(upper_l >= Math.max(sigma, tau) / 4.0);
    Preconditions.checkArgument(0.0 < epsilon && epsilon <= 1.0);
  }
}
