import { CiphertextMatrix } from "./ciphertext-matrix";

export class ObliviousTransferResponseJSON {

  constructor(public readonly bold_b: string[],
              public readonly bold_upper_c: CiphertextMatrix,
              public readonly d: string) {
  }
}
