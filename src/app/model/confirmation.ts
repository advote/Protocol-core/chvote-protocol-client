import { NonInteractiveZkp } from "./non-interactive-zkp";
import { BigInteger } from "./big-integer";
import { ConfirmationJSON } from "./confirmation-json";

export class Confirmation {

  constructor(public readonly y_hat: BigInteger,
              public readonly pi: NonInteractiveZkp) {
  }

  toJSON(): ConfirmationJSON {
    return new ConfirmationJSON(
      this.y_hat.toBase64String(),
      this.pi.toJSON()
    );
  }
}
