import { EncryptionPublicKey } from '../model/encryption-public-key';
import { EncryptionPrivateKey } from '../model/encryption-private-key';
import { EncryptionGroup } from '../model/encryption-group';
import { Preconditions } from '../tools/preconditions';
import { BigInteger } from '../model/big-integer';
import { EncryptionKeyPair } from '../model/encryption-key-pair';
import { RandomGenerator } from './random-generator.algo';
import * as _ from 'underscore';

/**
 * Algorithms used during the key establishment part of the election preparation phase
 */
export class KeyEstablishment {

  constructor() {
  }

  /**
   * Algorithm 7.15: genKeyPair
   *
   * @param eg the encryption group for which we need a key pair
   * @return a newly, randomly generated EncryptionKeyPair
   */
  static generateKeyPair(eg: EncryptionGroup): EncryptionKeyPair {
    let sk: BigInteger = RandomGenerator.randomInZq(eg.q);
    let pk: BigInteger = eg.g.modPow(sk, eg.p);

    return new EncryptionKeyPair(new EncryptionPublicKey(pk, eg), new EncryptionPrivateKey(sk, eg));
  }

  /**
   * Algorithm 7.16: GetPublicKey
   *
   * @param publicKeys the set of public key shares that should be combined
   * @return the combined public key
   */
  static getPublicKey(publicKeys: EncryptionPublicKey[]): EncryptionPublicKey {
    let publicKey: BigInteger = BigInteger.ONE;
    let eg: EncryptionGroup = publicKeys[0].encryptionGroup;

    Preconditions.checkArgument(_.every(publicKeys, function (pk: EncryptionPublicKey) {
      return pk.encryptionGroup.equals(eg);
    }), 'All of the public keys should be defined within the same encryption group');

    for (let key of publicKeys) {
      publicKey = publicKey.mul(key.publicKey).mod(eg.p);
    }
    return new EncryptionPublicKey(publicKey, eg);
  }

}