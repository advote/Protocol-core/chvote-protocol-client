import { EncryptionGroup } from "./encryption-group";
import { Preconditions } from "../tools/preconditions";
import { BigInteger } from "./big-integer";

export class PrimesCache {

  constructor(public readonly primes: BigInteger[],
              public readonly encryptionGroup: EncryptionGroup) {
    Preconditions.checkNotNull(primes);
    Preconditions.checkNotNull(encryptionGroup);
    primes.forEach(prime => Preconditions.checkArgument(PrimesCache.isMember(prime, encryptionGroup)));
  }

  private static isMember(x: BigInteger, encryptionGroup: EncryptionGroup): boolean {
    return x.cmp(BigInteger.ONE) >= 0
      && x.cmp(encryptionGroup.p) < 0
      && x.legendre(encryptionGroup.p) == 1;
  }
}
