import { NgModule } from '@angular/core';
import { Blake2Module } from '@protocoder/blake2';

@NgModule({
  imports: [
    Blake2Module
  ],
  declarations: []
})
export class ProtocolClientModule {
}
