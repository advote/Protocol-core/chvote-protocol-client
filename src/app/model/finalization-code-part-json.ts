import { BigIntPairJSON } from "./big-int-pair-json";

export class FinalizationCodePartJSON {

  constructor(public readonly upper_f: string,
              public readonly z: BigIntPairJSON) {
  }
}
