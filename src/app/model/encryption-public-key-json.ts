import { EncryptionGroupJSON } from "./encryption-group-json";

export class EncryptionPublicKeyJSON {

  constructor(public readonly publicKey: string,
              public readonly encryptionGroup: EncryptionGroupJSON) {
  }
}
