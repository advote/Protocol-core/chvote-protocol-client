export class BigIntPairJSON {

  constructor(public readonly left: string,
              public readonly right: string) {
  }

}
