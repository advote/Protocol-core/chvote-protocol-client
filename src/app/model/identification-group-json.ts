export class IdentificationGroupJSON {

  constructor(public readonly p_hat: string,
              public readonly q_hat: string,
              public readonly g_hat: string) {
  }
}
