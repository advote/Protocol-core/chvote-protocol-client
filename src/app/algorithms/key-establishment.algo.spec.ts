import { EncryptionPublicKey } from "../model/encryption-public-key";
import { EncryptionGroup } from "../model/encryption-group";
import { KeyEstablishment } from "./key-establishment.algo";
import { BigInteger } from "../model/big-integer";
import { EncryptionKeyPair } from "../model/encryption-key-pair";
import { RandomGenerator } from "./random-generator.algo";

describe('KeyEstablishment', () => {

  const THREE: BigInteger = new BigInteger(3);
  const FOUR: BigInteger = new BigInteger(4);
  const FIVE: BigInteger = new BigInteger(5);
  const ELEVEN: BigInteger = new BigInteger(11);

  it('generateKeyPair', () => {
    // given
    let encryptionGroup: EncryptionGroup = new EncryptionGroup(ELEVEN, FIVE, THREE, FOUR);
    spyOn(RandomGenerator, 'randomInZq').and.returnValue(THREE);
    // when
    let keyPair: EncryptionKeyPair = KeyEstablishment.generateKeyPair(encryptionGroup);
    // then
    expect(keyPair.encryptionPrivateKey.privateKey.equals(THREE)).toBeTruthy(`private key: expected ${keyPair.encryptionPrivateKey.privateKey.toHexString()} to equal 03`);
    expect(keyPair.encryptionPublicKey.publicKey.equals(FIVE)).toBeTruthy(`public key: expected ${keyPair.encryptionPublicKey.publicKey.toHexString()} to equal 05`);
  });

  it('getPublicKey', () => {
    // given
    let encryptionGroup: EncryptionGroup = new EncryptionGroup(ELEVEN, FIVE, THREE, FOUR);
    let pubKeys: EncryptionPublicKey[] = [
      new EncryptionPublicKey(FIVE, encryptionGroup),
      new EncryptionPublicKey(THREE, encryptionGroup)
    ];
    // when
    let publicKey: EncryptionPublicKey = KeyEstablishment.getPublicKey(pubKeys);
    // then
    expect(publicKey.encryptionGroup.equals(encryptionGroup)).toBeTruthy(`expected ${publicKey.encryptionGroup.toString()} to equal ${encryptionGroup.toString()}`);
    expect(publicKey.publicKey.equals(FOUR)).toBeTruthy(`bold_r: expected ${publicKey.publicKey.toHexString()} to equal 04`);
  });
});
