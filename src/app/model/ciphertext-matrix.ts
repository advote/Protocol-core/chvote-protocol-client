import { Map } from "immutable";
import { Preconditions } from "../tools/preconditions";

export class CiphertextMatrix {

  public readonly valuesMatrix: Map<number, CiphertextArray>;

  constructor(valuesMap: Map<number, Map<number, Uint8Array>>) {
    let map = Map<number, CiphertextArray>();
    valuesMap.forEach((value: Map<number, Uint8Array>, key: number) => {
      map = map.set(key, new CiphertextArray(value));
    });
    this.valuesMatrix = map;
  }

  /**
   * Retrieves the byte array for the provided coordinates
   *
   * @param i the first coordinate
   * @param j the second coordinate
   *
   * @return the byte array at the provided coordintates
   *
   * @throws IllegalArgumentException if no byte array exists at the given position
   */
  get(i: number, j: number): Uint8Array {
    Preconditions.checkArgument(this.valuesMatrix.has(i), `no values for index i = ${i}`);
    return this.valuesMatrix.get(i).get(j);
  }

  static fromJSON(json): CiphertextMatrix {
    let map: Map<number, Map<number, Uint8Array>> = Map<number, Map<number, Uint8Array>>();

    for (let i in json.valuesMap) {
      let keyMap: Map<number, Uint8Array> = Map<number, Uint8Array>();
      for (let j in json.valuesMap[i]) {
        keyMap = keyMap.set(parseInt(j), this.base64ToUint8Array(json.valuesMap[i][j]));
      }
      map = map.set(parseInt(i), keyMap);
    }

    return new CiphertextMatrix(map);
  }

  static base64ToUint8Array(base64: string): Uint8Array {
    let raw = window.atob(base64);
    let rawLength = raw.length;
    let array = new Uint8Array(new ArrayBuffer(rawLength));

    for (let i = 0; i < rawLength; i++) {
      array[i] = raw.charCodeAt(i);
    }
    return array;
  }
}

export class CiphertextArray {

  constructor(public readonly ciphertextMap: Map<number, Uint8Array>) {
  }

  get(j: number): Uint8Array {
    Preconditions.checkArgument(this.ciphertextMap.has(j), `no values for index i = ${j}`);
    return this.ciphertextMap.get(j);
  }
}
