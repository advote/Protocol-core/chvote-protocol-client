# CHVote JS Protocol Client

Angular library exposing the CHVote algrotihtms to be used by the voting client.

## Build

Run `ng build` to build the project using the [ng-packagr](https://github.com/dherges/ng-packagr) plugin. 
The build artifacts will be stored in the `dist/` directory and a `dist.tgz` archive will be created.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Usage

Simply import the `ProtocolClientModule` in your project.
You will also need to include the [Verificatum JavaScript Cryptographic library (VJSC)](https://www.verificatum.com/html/download_vjsc.html) to your assets directory.
