import { EncryptionGroup } from "./encryption-group";
import { BigInteger } from "./big-integer";
import { EncryptionPublicKeyJSON } from "./encryption-public-key-json";

export class EncryptionPublicKey {

  constructor(public readonly publicKey: BigInteger,
              public readonly encryptionGroup: EncryptionGroup) {
  }

  static fromJSON(json: EncryptionPublicKeyJSON | string): EncryptionPublicKey {
    if (typeof json === 'string') {
      return JSON.parse(json, EncryptionPublicKey.reviver);

    } else {
      let publicKeys = Object.create(EncryptionPublicKey.prototype);

      return Object.assign(publicKeys, json, {
        publicKey: new BigInteger(json.publicKey),
        encryptionGroup: EncryptionGroup.fromJSON(json.encryptionGroup)
      });
    }
  }

  static reviver(key: string, value: any): any {
    return key === "" ? EncryptionPublicKey.fromJSON(value) : value;
  }
}
