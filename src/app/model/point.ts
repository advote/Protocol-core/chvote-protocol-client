import { BigInteger } from "./big-integer";

export class Point {

  constructor(public readonly x: BigInteger,
              public readonly y: BigInteger) {
  }
}
