import { EncryptionGroup } from "../model/encryption-group";
import { BigInteger } from "../model/big-integer";
import RandomDevice = verificatum.crypto.RandomDevice;

export class RandomGenerator {

  private static MIN_VALUE: number = -128;
  private static MAX_VALUE: number = 127;
  private static MAX_ITERATIONS: number = RandomGenerator.MAX_VALUE - RandomGenerator.MIN_VALUE;

  constructor() {
  }

  /**
   * Draw at random from Z_q
   *
   * @param q the exclusive upperbound to draw from
   *
   * @return an element picked at random from a uniform distribution of Z_q
   */
  public static randomInZq(q: BigInteger): BigInteger {
    return RandomGenerator.randomBigInteger(q.sub(BigInteger.ONE));
  }

  /**
   * Draw an element at random from the group G_q
   *
   * @param encryptionGroup the encryption group to draw from
   *
   * @return an element picked at random
   */
  public static randomInGq(encryptionGroup: EncryptionGroup): BigInteger {
    let x: BigInteger = RandomGenerator.randomInZq(encryptionGroup.q);
    return x.modPow(BigInteger.TWO, encryptionGroup.p);
  }

  /**
   * @param upperbound exclusive upperbound
   *
   * @return a random BigInteger in range [0, upperbound)
   */
  static randomBigInteger(upperbound: BigInteger): BigInteger {
    for (let i = 0; i < RandomGenerator.MAX_ITERATIONS; i++) {
      let x: BigInteger = new BigInteger(upperbound.bitLength(), new RandomDevice());
      if (x.cmp(upperbound) < 0) {
        return x;
      }
    }

    // If we fail to get a value within range for MAX_ITERATIONS, get a value with lower bitCount
    return new BigInteger(upperbound.bitLength() - 1, new RandomDevice());
  }
}
