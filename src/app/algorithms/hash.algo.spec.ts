import { Hash } from './hash.algo';
import { Blake2bService, UtilService } from "@protocoder/blake2";
import { BigInteger } from "../model/big-integer";
import { BigIntPair } from "../model/big-int-pair";

describe('Hash', () => {

  let hash: Hash;

  beforeAll(() => {
    hash = new Hash(new Blake2bService(), 32);
  });

  it('hash_L(string)', () => {
    expect(hash.hash_L('test')).toEqual(new Uint8Array([146, 139, 32, 54, 105, 67, 226, 175, 209, 30, 188, 14, 174, 46, 83, 169, 59, 241, 119, 164, 252, 243, 91, 204, 100, 213, 3, 112, 78, 101, 226, 2]));
  });

  it('hash_L(BigInteger)', () => {
    expect(UtilService.toHex(hash.hash_L(new BigInteger(12345)))).toEqual('afd7f87a3d7a6578e1d8f62efbde0f293b8ee7fdbc80ffa10577adb159137770');
  });

  it('recHash_L(BigInteger[])', () => {
    expect(UtilService.toHex(hash.recHash_L([new BigInteger(12345), new BigInteger(67890)]))).toEqual('cdef2e19359bb8454a6ef7b6f3e7ac2412d9b47b4bad5e4650d297eed5f8c9c5');
  });

  it('hash_L(BigIntPair)', () => {
    let bigIntPair: BigIntPair = new BigIntPair(
      new BigInteger(12345),
      new BigInteger(67890)
    );
    expect(UtilService.toHex(hash.hash_L(bigIntPair))).toEqual('cdef2e19359bb8454a6ef7b6f3e7ac2412d9b47b4bad5e4650d297eed5f8c9c5');
  });

  // recHash_L(Object[])
  // [
  //   ['string, LargeInteger, Uint8Array', ['test', new BigInteger(42), new Uint8Array([0xCC, 0xFF])], new Uint8Array([29, 118, 86, 103, 88, 165, 182, 191, 197, 97, 241, 201, 54, 216, 252, 134, 181, 180, 46, 162, 42, 177, 218, 191, 64, 210, 73, 210, 125, 217, 6, 64, 31, 222, 20, 126, 83, 244, 76, 16, 61, 208, 42, 37, 73, 22, 190, 17, 62, 81, 222, 16, 119, 169, 70, 163, 160, 193, 39, 43, 155, 52, 132, 55])],
  //   ['string, LargeInteger[], Uint8Array', ['test', [new BigInteger(42)], new Uint8Array([0xCC, 0xFF])], new Uint8Array([29, 118, 86, 103, 88, 165, 182, 191, 197, 97, 241, 201, 54, 216, 252, 134, 181, 180, 46, 162, 42, 177, 218, 191, 64, 210, 73, 210, 125, 217, 6, 64, 31, 222, 20, 126, 83, 244, 76, 16, 61, 208, 42, 37, 73, 22, 190, 17, 62, 81, 222, 16, 119, 169, 70, 163, 160, 193, 39, 43, 155, 52, 132, 55])]
  // ].forEach(([usecase, objects, expected]) => {
  //   it(`recHash_L(${usecase})`, () => {
  //     expect(hash.recHash_L(objects)).toEqual(<Uint8Array>expected);
  //   });
  // });
});
